var files_path = Drupal.settings.wysiwyg_choose_image_style.host_info.path;

(function() {
	// Load plugin specific language pack
	//tinymce.PluginManager.requireLangPack('example');
	tinymce.create('tinymce.plugins.drupalimagestyle', {
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {

			// Add a node change handler, enables/disables and sets the selected value accordingly
			ed.onNodeChange.add(function(ed, cm, n) {
				
				var nodeIsIMG = n.nodeName == 'IMG';
				var nodeIsLocal = false;
				
				
				if(nodeIsIMG)
				{

					var pathInfo = parse_url(n.src);
					
					var curStyle = null;
					//check if files resides within the files folder
					if(n.src.indexOf(files_path)!=-1)
					{
						//check if the host is equal to the drupal host
						nodeIsLocal = pathInfo.host == Drupal.settings.wysiwyg_choose_image_style.host_info.host;
	        			
	        			//detect the current style (if any)
						curStyle = detectStyle(pathInfo.path);
					}
        			
        			var select = cm.get('imagestyleselect');
        			if(curStyle==null)
        				select.selectByIndex(-1);
        			else
        				select.select(curStyle);
        			
				}
				
				cm.setDisabled('imagestyleselect', !(nodeIsIMG && nodeIsLocal) );
				
			});
		},

		/**
		 * Creates control instances based in the incomming name. This method is normally not
		 * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
		 * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
		 * method can be used to create those.
		 *
		 * @param {String} n Name of the control to create.
		 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
		 * @return {tinymce.ui.Control} New control instance or null if no control was created.
		 */
		createControl : function(n, cm) {
			switch (n) {
	            case 'imagestyleselect':
	            	var thetitle = countProps(Drupal.settings.wysiwyg_choose_image_style.image_styles)>0?Drupal.settings.wysiwyg_choose_image_style.image_style_strings.title:Drupal.settings.wysiwyg_choose_image_style.image_style_strings.nostyles;
	                var mlb = cm.createListBox('imagestyleselect', {
	                	// title should be set with language support
	                     title : thetitle,
	                     onselect : function(v)
	                     {
	                		var ed = tinyMCE.activeEditor;
	                		var imgNode = ed.selection.getNode();
	                		
	                		if(imgNode.nodeName=='IMG')
	         				{
	                			var pStyle = null;
	                			var src = imgNode.getAttribute("data-mce-src");
	                			var pathInfo = parse_url(src);
	                			
	                			//detect the current style
	                			pStyle = detectStyle(pathInfo.path);
	                			
	                			//prepare the sites string..
	                			var sitesString = files_path;
	                			var sitesLength = sitesString.length;
	                			var sitesIndex = src.indexOf(sitesString);
	                			
	                			// make extra sure this will only work on images, that are in the files folder
	                			if(sitesIndex!=-1)
	                			{
		                			var currentStyleString = pStyle!=null?"styles/"+pStyle+"/public/":"";
		                			var httpString = src.substr(0,sitesIndex);
		                			var stylesString = src.substr(sitesIndex+sitesLength,currentStyleString.length);
		                			var imageString = src.substr(sitesIndex+sitesLength+currentStyleString.length);
	
		                			var newStyleString = v!=""?"styles/"+v+"/public/":"";
		                			
		                			// remove any width and height attribute to make sure any scaling image styles is in the correct scale
		                			imgNode.removeAttribute("width");
		                			imgNode.removeAttribute("height");
		                			
		                			var newSrc = httpString + sitesString + newStyleString + imageString;
		                			imgNode.setAttribute("data-mce-src",newSrc);
		                			imgNode.src = newSrc;
	                			}

	                			//attempt to redraw the ui (still no luck in redrawing the image corner handles)
	                			ed.undoManager.add();
	                			ed.focus(false);
	                			ed.nodeChanged();
	                			ed.save();
	         				}
	                     }
	                });
                
	                // Add some values to the list box
	                for(var i in Drupal.settings.wysiwyg_choose_image_style.image_styles)
	                {
	                	mlb.add(Drupal.settings.wysiwyg_choose_image_style.image_styles[i].name, Drupal.settings.wysiwyg_choose_image_style.image_styles[i].name);
	                }

	                // Return the new listbox instance
	                return mlb;
	
	            
			}

			return null;
		},

		/**
		 * Returns information about the plugin as a name/value array.
		 * The current keys are longname, author, authorurl, infourl and version.
		 *
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
				longname : 'Drupal Image Style Plugin',
				author : 'prinds',
				authorurl : 'http://prinds.com',
				infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/example',
				version : "1.0"
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('drupalimagestyle', tinymce.plugins.drupalimagestyle);
})();

function detectStyle(path)
{
	var curStyle = null;
	var rxString = "^"+files_path+"styles/([a-z0-9_]+)/(.+)";
	var rx = new RegExp(rxString,"i");
	var result = path.match(rx);
	if(result)
	{
		curStyle = result[1];
	}
	return curStyle;
}

function countProps(obj) {
    var count = 0;

    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
                ++count;
    }

    return count;
}

function parse_url (str, component) {
    // Parse a URL and return its components  
    // 
    // version: 1103.1210
    // discuss at: http://phpjs.org/functions/parse_url    // +      original by: Steven Levithan (http://blog.stevenlevithan.com)
    // + reimplemented by: Brett Zamir (http://brett-zamir.me)
    // + input by: Lorenzo Pisani
    // + input by: Tony
    // + improved by: Brett Zamir (http://brett-zamir.me)    // %          note: Based on http://stevenlevithan.com/demo/parseuri/js/assets/parseuri.js
    // %          note: blog post at http://blog.stevenlevithan.com/archives/parseuri
    // %          note: demo at http://stevenlevithan.com/demo/parseuri/js/assets/parseuri.js
    // %          note: Does not replace invalid characters with '_' as in PHP, nor does it return false with
    // %          note: a seriously malformed URL.    // %          note: Besides function name, is essentially the same as parseUri as well as our allowing
    // %          note: an extra slash after the scheme/protocol (to allow file:/// as in PHP)
    // *     example 1: parse_url('http://username:password@hostname/path?arg=value#anchor');
    // *     returns 1: {scheme: 'http', host: 'hostname', user: 'username', pass: 'password', path: '/path', query: 'arg=value', fragment: 'anchor'}
    var key = ['source', 'scheme', 'authority', 'userInfo', 'user', 'pass', 'host', 'port',                         'relative', 'path', 'directory', 'file', 'query', 'fragment'],
        ini = (this.php_js && this.php_js.ini) || {},
        mode = (ini['phpjs.parse_url.mode'] && 
            ini['phpjs.parse_url.mode'].local_value) || 'php',
        parser = {            php: /^(?:([^:\/?#]+):)?(?:\/\/()(?:(?:()(?:([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?()(?:(()(?:(?:[^?#\/]*\/)*)()(?:[^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
            strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
            loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // Added one optional slash to post-scheme to catch file:/// (should restrict this)
        };
     var m = parser[mode].exec(str),
        uri = {},
        i = 14;
    while (i--) {
        if (m[i]) {          uri[key[i]] = m[i];  
        }
    }
 
    if (component) {        return uri[component.replace('PHP_URL_', '').toLowerCase()];
    }
    if (mode !== 'php') {
        var name = (ini['phpjs.parse_url.queryKey'] && 
                ini['phpjs.parse_url.queryKey'].local_value) || 'queryKey';        parser = /(?:^|&)([^&=]*)=?([^&]*)/g;
        uri[name] = {};
        uri[key[12]].replace(parser, function ($0, $1, $2) {
            if ($1) {uri[name][$1] = $2;}
        });    }
    delete uri.source;
    return uri;
}
